# Mosaic

This program implements the game Mosaic, sometimes also called Fill-A-Pix or Nurie, depending on the vendor.
It has the ability to generate puzzles based on generators (WIP) and read and write hint files for exchanging.

## The game

The goal of the puzzle is to fill in every square in black or white, depending on the hints given as numbers in certain fields.
This number denotes the number of black fields in the 3x3 square around the hint.


## Work in Progress

Currently no puzzle generator based on image generation is working in a satisfactory manner.
Also more image generators are always welcome.

Another possible addition would be saving and loading the current state of some/the current puzzle.


## Hint files

The easiest way to generate a hint file is to run "Generate > Generate Hints" which generates hints for
the current state of black squares (gray squares are counted as white), which then can be saved under
"Puzzle > Save hints".

The hint file format is as follows:</br>
The two numbers on the first line are width and height.
The remaining lines contain the hint of the i-th field in the row, or whitespace if no hint is given.

