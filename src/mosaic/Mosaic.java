package mosaic;

import java.util.ArrayList;

import mosaic.util.Bitset2D;

/**
 * The mosaic class contains all logic necessary to hold field colors and verify
 * hints.
 * 
 * @author Timon
 *
 */

public class Mosaic {
	private Field[][] fields;
	private int width;
	private int height;

	public Mosaic(int w, int h) {
		this.width = w;
		this.height = h;
		this.fields = new Field[h][w];
		for (int y = 0; y < h; ++y) {
			for (int x = 0; x < w; ++x) {
				fields[y][x] = new Field();
			}
		}
	}

	public int getWidth() {
		return this.width;
	}

	public int getHeight() {
		return this.height;
	}

	public MColor colorAt(int x, int y) {
		return this.fields[y][x].color;
	}

	public int numberAt(int x, int y) {
		return this.fields[y][x].number;
	}

	/**
	 * Updates the color for a specific position. Also updates all the number of
	 * black fields for the surrounding fields.
	 * 
	 * @param x - The x position of the field to update
	 * @param y - The y position of the field to update
	 * @param color - The color to set the field to
	 */
	public void setColorAt(int x, int y, MColor color) {
		MColor oldc = this.fields[y][x].color;
		if (oldc != color) {
			int diff = 0;
			if (color == MColor.Black) {
				diff = 1;
			} else if (oldc == MColor.Black) {
				diff = -1;
			}
			this.fields[y][x].color = color;

			for (int i = Math.max(x - 1, 0); i < Math.min(x + 2, width); ++i) {
				for (int j = Math.max(y - 1, 0); j < Math.min(y + 2, height); ++j) {
					this.fields[j][i].number += diff;
				}
			}

		}
	}

	/**
	 * Creates a Mosaic from a given bitset.
	 * 
	 * @param input - The input bitset
	 * @param width - The width of the mosaic
	 * @param height - The height of the mosaic
	 * @return The mosaic defined by the bitset
	 */
	public static Mosaic FromBitset(Bitset2D input, int width, int height) {
		Mosaic res = new Mosaic(width, height);
		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				res.setColorAt(x, y, input.get(x, y) ? MColor.White : MColor.Black);
			}
		}
		return res;
	}

	/**
	 * Verifies the hints and returns an {@code ArrayList} with positions that
	 * are wrong.
	 * 
	 * @param hints - The hints to verify
	 * @return An {@code ArrayList} of positions that are wrong.
	 */
	public ArrayList<int[]> verifyHints(int[][] hints) {
		ArrayList<int[]> wrongPositions = new ArrayList<>();
		assert (hints.length == this.height);
		for (int y = 0; y < this.height; ++y) {
			assert (hints[y].length == this.width);
			for (int x = 0; x < this.width; ++x) {
				if (hints[y][x] != -1 && this.numberAt(x, y) != hints[y][x]) {
					wrongPositions.add(new int[] { x, y });
				}
			}
		}
		return wrongPositions;
	}
}
