package mosaic;

import java.util.ArrayDeque;

public class ActionQueue {
	private ArrayDeque<Action> actionStack;
	private ArrayDeque<Action> redoStack;
	
	public ActionQueue() {
		actionStack = new ArrayDeque<>();
		redoStack = new ArrayDeque<>();
	}
	
	private void addAction(Action a) {
		actionStack.push(a);
		redoStack.clear();
	}
	
	public void drewAt(int x, int y, MColor oldColor, MColor newColor) {
		addAction(new DrawAction(x, y, oldColor, newColor));
	}
	
	public void clear() {
		actionStack.clear();
		redoStack.clear();
	}
	
	public void undo(Mosaic mosaic, int[][] hints) {
		if (!actionStack.isEmpty()) {
			Action a = actionStack.pop();
			a.undo(mosaic, hints);
			redoStack.push(a);
		}
	}
	
	public void redo(Mosaic mosaic, int[][] hints) {
		if (!redoStack.isEmpty()) {
			Action a = redoStack.pop();
			a.redo(mosaic, hints);
			actionStack.push(a);
		}
	}
	
	private interface Action {
		void undo(Mosaic mosaic, int[][] hints);
		
		void redo(Mosaic mosaic, int[][] hints);
	}
	
	private class DrawAction implements Action {
		private int x, y;
		private MColor oldColor;
		private MColor newColor;
		
		public DrawAction(int x, int y, MColor oldColor, MColor newColor) {
			this.x = x;
			this.y = y;
			this.oldColor = oldColor;
			this.newColor = newColor;
		}
		
		@Override
		public void undo(Mosaic mosaic, int[][] hints) {
			mosaic.setColorAt(x, y, oldColor);
		}
		
		@Override
		public void redo(Mosaic mosaic, int[][] hints) {
			mosaic.setColorAt(x, y, newColor);
		}
	}
}
