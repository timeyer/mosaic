package mosaic.imagegenerators;

import mosaic.util.Bitset2D;
import mosaic.util.Complex;

public class DFTGenerator implements ImageGenerator {
	private static Complex[][] Four_r, Four_c;

	@Override
	public Bitset2D generateImage(int width, int height) {
		double[][] freqs = new double[height][width];
		int rfs = (int) ((Math.random() + 0.2) * (width * height) / 10);
		for (int i = 0; i < rfs; ++i)
			freqs[(int) (Math.random() * height)][(int) (Math.random() * width)] = 1;

		Four_r = computeFourier(width);
		Four_c = computeFourier(height);

		// transform rows
		Complex[][] tmp = new Complex[height][width];
		for (int j = 0; j < height; ++j)
			tmp[j] = dft(Four_r, rtoc(freqs[j]));

		// transform cols
		double avg = 0;
		double[][] transf = new double[height][width];
		for (int x = 0; x < width; ++x) {
			Complex[] col = new Complex[height];
			for (int y = 0; y < height; ++y)
				col[y] = tmp[y][x];
			double[] t = ctor(dft(Four_c, col));
			for (int y = 0; y < height; ++y) {
				transf[y][x] = t[y];
				avg += t[y];
			}
		}

		Bitset2D image = new Bitset2D(width, height);
		avg /= width * height;
		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				if (transf[y][x] > avg)
					image.set(x, y);
			}
		}

		return image;
	}

	public Complex[][] computeFourier(int n) {
		Complex[][] F = new Complex[n][n];
		for (int j = 0; j < n; ++j) {
			for (int i = 0; i < n; ++i) {
				F[j][i] = (new Complex(0, -2 * Math.PI / n)).mul(i * j).exp();
			}
		}
		return F;
	}

	public Complex[] dft(Complex[][] F, Complex[] arr) {
		int n = arr.length;
		Complex[] res = new Complex[n];
		for (int j = 0; j < n; ++j) {
			res[j] = new Complex(0, 0);
			for (int i = 0; i < n; ++i) {
				res[j].iadd(arr[i].mul(F[j][i]));
			}
		}

		return res;
	}

	public Complex[] rtoc(double[] in) {
		Complex[] res = new Complex[in.length];
		for (int i = 0; i < in.length; ++i)
			res[i] = new Complex(in[i], 0);
		return res;
	}

	public double[] ctor(Complex[] in) {
		double[] res = new double[in.length];
		for (int i = 0; i < in.length; ++i)
			res[i] = Math.sqrt(in[i].real * in[i].real + in[i].imag * in[i].imag);
		return res;
	}

	public void mulVec(double[] v, double f) {
		for (int i = 0; i < v.length; ++i)
			v[i] *= f;
	}
}
