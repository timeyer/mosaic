package mosaic.imagegenerators;

import java.util.ArrayList;

import mosaic.util.Vector2;

public class LizardGenerator extends LineGenerator {
	private ArrayList<Line> lines;
	
	public LizardGenerator(int width, int height) {
		lines = new ArrayList<>();
		Vector2 head = new Vector2(width / 2., height / 2.);
		Vector2 tail = head;
		double ang = Math.PI * 2 * Math.random();
		double angt = ang - Math.PI;
		double bend = (Math.random() - 0.5) / 4.; 
		
		double scale = Math.min(width, height) / 20;
		double w = scale * (1.5 + Math.random() / 2);
		
		// body
		int bl = (int)(2 * Math.random() + 1);
		for (int i = 0; i < bl; ++i) {
			Vector2 head2 = head.add(Vector2.FromPolar(scale * (1.5 + Math.random()), ang));
			Vector2 tail2 = tail.add(Vector2.FromPolar(scale * (1.5 + Math.random()), angt));
			lines.add(new Line(head, head2, w));
			lines.add(new Line(tail, tail2, w));
			ang += bend;
			angt -= bend;
			w *= 0.85;
			head = head2;
			tail = tail2;
		}
		
		// arms
		genArm(scale, lines, head, ang - Math.PI / 3);
		genArm(scale, lines, head, ang + Math.PI / 3);
		genArm(scale, lines, tail, angt - 2 * Math.PI / 3);
		genArm(scale, lines, tail, angt + 2 * Math.PI / 3);
		
		// tail
		int tl = (int)(2 * Math.random() + 4);
		bend = (Math.random() - 0.5) / 2;
		w = scale * (1 + Math.random() / 2);
		for (int i = 0; i < tl; ++i) {
			Vector2 tail2 = tail.add(Vector2.FromPolar(scale * (2 + Math.random()), angt));
			lines.add(new Line(tail, tail2, w));
			tail = tail2;
			angt += bend;
			w *= 0.85;
		}
		
		// head
		Vector2 neck = head.add(Vector2.FromPolar(scale * (2 + Math.random()), ang));
		lines.add(new Line(head, neck, scale * 1.3));
		//Vector2 face = neck.add(Vector2.FromPolar(scale * 1.2, ang));
		lines.add(new Line(neck, neck, scale * 2));
		lines.add(new Line(neck, neck.add(Vector2.FromPolar(scale * (3 + Math.random()), ang)), scale * 1));
	}
	
	@Override
	public ArrayList<Line> getLines() {
		return lines;
	}
	
	private void genArm(double scale, ArrayList<Line> lines, Vector2 start, double angle) {
		Vector2 hand = start.add(Vector2.FromPolar(scale * (4 + Math.random()), angle));
		lines.add(new Line(start, hand, scale * 1));
		double l = scale * (1 + Math.random() / 2);
		lines.add(new Line(hand, hand.add(Vector2.FromPolar(l, angle - Math.PI / 3)), scale * 0.4));
		lines.add(new Line(hand, hand.add(Vector2.FromPolar(l, angle)), scale * 0.6));
		lines.add(new Line(hand, hand.add(Vector2.FromPolar(l, angle + Math.PI / 3)), scale * 0.4));
	}
}
