package mosaic.imagegenerators;

import java.awt.Graphics;
import java.util.ArrayList;

import mosaic.util.Bitset2D;
import mosaic.util.Vector2;

public abstract class LineGenerator implements ImageGenerator {

	public abstract ArrayList<Line> getLines();

	@Override
	public Bitset2D generateImage(int width, int height) {
		Bitset2D res = new Bitset2D(width, height);
		res.setAll();
		for (Line l : getLines()) {
			l.drawToBitset(res, width, height);
		}
		return res;
	}

	public class Line {
		private double startx, starty, endx, endy;
		private double thickness;

		public Line(double startx, double endx, double starty, double endy, double thickness) {
			this.startx = startx;
			this.endx = endx;
			this.starty = starty;
			this.endy = endy;
			this.thickness = thickness;
		}

		public void drawToBitset(Bitset2D bitset, int w, int h) {
			// we add 0.25 to width and height because double might round below
			// width/height
			// and we can no longer get full width and height. (zero is not
			// rounded)
			int top = (int) Math.max(0., Math.min(this.starty, this.endy) - this.thickness);
			int bottom = (int) Math.min((double) h + 0.25, Math.max(this.starty, this.endy) + (int) this.thickness + 1);
			int left = (int) Math.max(0., Math.min(this.startx, this.endx) - (int) this.thickness);
			int right = (int) Math.min((double) w + 0.25, Math.max(this.startx, this.endx) + (int) this.thickness + 1);

			// normal vector
			double nx = this.startx - this.endx;
			double ny = this.endy - this.starty;
			double nl = Math.sqrt(nx * nx + ny * ny);

			// direction vector
			double ax = this.endx - this.startx;
			double ay = this.endy - this.starty;
			double al = Math.sqrt(ax * ax + ay * ay);

			for (int y = top; y < bottom; ++y) {
				for (int x = left; x < right; ++x) {
					double px = x - this.startx;
					double py = y - this.starty;

					// How far we need to advance on the direction vector.
					// Needs to be between 0 and the length of the direction
					// vector.
					double t = (ax * px + ay * py) / al;

					// Hessian normal form for the distance from the line.
					double d = (nx * px + ny * py) / nl;

					if ((t >= 0 && t <= al && Math.abs(d) <= this.thickness)
							|| Math.sqrt(px * px + py * py) <= this.thickness
							|| Math
									.sqrt(
											(x - this.endx) * (x - this.endx)
													+ (y - this.endy) * (y - this.endy)) <= this.thickness)
						bitset.unset(x, y);
				}
			}
		}

		public void drawToGraphics(Graphics g, int cellSize) {
			g
					.drawLine(
							(int) ((this.startx + 0.5) * cellSize),
							(int) ((this.starty + 0.5) * cellSize),
							(int) ((this.endx + 0.5) * cellSize),
							(int) ((this.endy + 0.5) * cellSize));
		}

		public Line(Vector2 start, Vector2 end, double thickness) {
			this.startx = start.getX();
			this.starty = start.getY();
			this.endx = end.getX();
			this.endy = end.getY();
			this.thickness = thickness;
		}
	}
}
