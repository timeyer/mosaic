package mosaic.imagegenerators;

import mosaic.util.Bitset2D;

public class TNoiseGenerator implements ImageGenerator {

	@Override
	public Bitset2D generateImage(int width, int height) {

		// we go over the border to the right to make sure we can always
		// interpolate between in the next step
		double[][] nmap = new double[height + 2][width + 2];
		for (int j = 0; j < height + 2; j += 2) {
			for (int i = 0; i < width + 2; i += 2) {
				nmap[j][i] = Math.random();
			}
		}
		
		for (int j = 1; j < height + 2; j += 2) {
			for (int i = 1; i < width + 2; i += 2) {
				nmap[j][i] = Math.random();
			}
		}

		for (int j = 1; j < height + 1; ++j) {
			for (int i = 1 + (j % 2); i < width; i += 2) {
				double min = Math.min(Math.min(nmap[j][i + 1], nmap[j + 1][i]), Math.min(nmap[j][i-1], nmap[j-1][i]));
				double max = Math.max(Math.max(nmap[j][i + 1], nmap[j + 1][i]), Math.max(nmap[j][i-1], nmap[j-1][i]));
				nmap[j][i] = min + Math.random() * (max - min);
			}
		}

		Bitset2D res = new Bitset2D(width, height);
		for (int j = 0; j < height; ++j) {
			for (int i = 0; i < width; ++i) {
				res.set(i, j, nmap[j+1][i+1] < 0.5);
			}
		}

		return res;
	}
}
