package mosaic.imagegenerators;

import mosaic.util.Bitset2D;

public class CloudGenerator implements ImageGenerator {

	@Override
	public Bitset2D generateImage(int width, int height) {
		Bitset2D bs = new Bitset2D(width, height);
		bs.setAll();
		int centerX = (int) (width * (0.25 + Math.random() / 2));
		int centerY = (int) (height * (0.25 + Math.random() / 2));
		int cWidth = (int) (width * (0.3 + Math.random() / 3));
		int cHeight = (int) Math.min(height * (0.3 + Math.random() / 3), cWidth * 0.8);
		int cLeft = centerX - cWidth / 2;
		int cTop = centerY - cHeight / 2;
		// System.out.println(cLeft + ", " + cTop);

		int cCount = (int) (8 + Math.random() * 8);
		for (int i = 0; i < cCount; ++i) {
			drawCloud(
					bs,
					(int) (cLeft + Math.random() * cWidth),
					(int) (cTop + Math.random() * cHeight),
					width,
					height,
					Math.min(cWidth, cHeight) / 4 * (Math.random() + 1));
		}
		return bs;
	}

	private void drawCloud(Bitset2D bitset, int x, int y, int width, int height, double r) {
		boolean genBorder = Math.random() >= 0.3;
		for (int j = Math.max(0, y - (int) (r + 1)); j < Math.min(width, y + (int) (r + 1)); ++j) {
			for (int i = Math.max(0, x - (int) (r + 1)); i < Math.max(height, x + (int) (r + 1)); ++i) {
				if (Math.sqrt((x - i) * (x - i) + (y - j) * (y - j)) < r) {
					bitset.unset(i, j);
				} else if (genBorder && Math.sqrt((x - 0.8 - i) * (x - 0.8 - i) + (y - 0.8 - j) * (y - 0.8 - j)) < r) {
					bitset.set(i, j);
				}
			}
		}
	}

}
