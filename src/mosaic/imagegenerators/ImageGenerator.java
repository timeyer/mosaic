package mosaic.imagegenerators;

import mosaic.util.Bitset2D;

public interface ImageGenerator {
	public Bitset2D generateImage(int width, int height);
}
