package mosaic;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.FileDialog;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.WindowEvent;
import java.awt.event.WindowListener;
import java.io.File;
import java.io.FilenameFilter;

import javax.swing.*;

import mosaic.imagegenerators.CloudGenerator;
import mosaic.imagegenerators.DFTGenerator;
import mosaic.imagegenerators.LizardGenerator;
import mosaic.imagegenerators.TNoiseGenerator;
import mosaic.util.StateIO;

/**
 * Mosaic
    Copyright (C) 2022  Timon Meyer

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 * Main class containing the GUI and puzzle runtime.
 * 
 * @author Timon
 *
 */

public class Main extends JFrame implements WindowListener {
	private static final long serialVersionUID = -4316603785537774078L;
	static Main window = new Main();
	private JScrollPane scroller;
	private JCheckBox showMistakes;
	private Puzzle puzzle;
	private DimensionDialog ddialog = new DimensionDialog(this);

	private JButton colorButton;
	private static MColor selectedColor = MColor.Black;

	private Main() {
		super("Mosaik");
		this.setPreferredSize(new Dimension(500, 500));
	}

	public static void main(String[] args) {
		window.createAndShowGUI();
		window.puzzle.load(StateIO.getDefaultFile());
	}

	public static void selectColor(MColor c) {
		selectedColor = c;
		window.colorButton.setBackground(colorForFieldColor(c));
	}

	public static MColor selectedColor() {
		return selectedColor;
	}

	public static Color colorForFieldColor(MColor c) {
		if (c == MColor.White)
			return Color.white;
		else if (c == MColor.Gray)
			return Color.lightGray;
		else
			return Color.black;
	}

	/**
	 * Rotates through the colors as follows: Gray -> Black -> White -> Gray
	 * 
	 * @param c - The current color
	 * @return The next color
	 */
	public static MColor nextColor(MColor c) {
		if (c == MColor.Gray)
			return MColor.Black;
		else if (c == MColor.Black)
			return MColor.White;
		else
			return MColor.Gray;
	}

	/**
	 * Sets the color of the frame around the game. Inspired by the UI at
	 * <a href="https://www.janko.at/Raetsel/Mosaik/index.htm">janko.at</a>.
	 * 
	 * @param c - The color of the frame around the puzzle.
	 */
	public static void setFrameColor(Color c) {
		if (window.scroller != null) {
			window.scroller.setBorder(BorderFactory.createMatteBorder(10, 10, 10, 10, c));
			window.repaint();
		}
	}

	/////////
	// GUI //
	/////////

	public void createAndShowGUI() {
		// setup listeners
		this.registerShortcuts();

		// menu bar
		JMenuBar menubar = new JMenuBar();
		this.setJMenuBar(menubar);

		// File menu

		JMenu puzzleMenu = new JMenu("Puzzle");
		menubar.add(puzzleMenu);

		JMenuItem saveState= new JMenuItem("Save state");
		saveState.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				FileDialog dialog = new FileDialog(window, "Select the file to write to.", FileDialog.SAVE);
				dialog.setFilenameFilter(new FilenameFilter() {
					@Override
					public boolean accept(File dir, String name) {
						return name.endsWith(".txt");
					}
				});
				dialog.setVisible(true);
				if (dialog.getFile() != null) {
					String filename = dialog.getFile();
					File f = new File(dialog.getDirectory(), filename.endsWith(".txt") ? filename : filename + ".txt");
					Main.this.puzzle.save(f, Main.this.ddialog.getDimensions());
				}
			}
		});
		puzzleMenu.add(saveState);

		JMenuItem loadState= new JMenuItem("Load state");
		loadState.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				FileDialog dialog = new FileDialog(window, "Select the state to load.", FileDialog.LOAD);
				dialog.setFilenameFilter(new FilenameFilter() {
					@Override
					public boolean accept(File dir, String name) {
						return name.endsWith(".txt");
					}
				});
				dialog.setVisible(true);
				if (dialog.getFile() != null) {
					File f = new File(dialog.getDirectory(), dialog.getFile());
					Main.this.puzzle.load(f);
				}
			}
		});
		puzzleMenu.add(loadState);
		
		JMenuItem saveHints = new JMenuItem("Save hints");
		saveHints.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				FileDialog dialog = new FileDialog(window, "Select the file to write to.", FileDialog.SAVE);
				dialog.setFilenameFilter(new FilenameFilter() {
					@Override
					public boolean accept(File dir, String name) {
						return name.endsWith(".txt");
					}
				});
				dialog.setVisible(true);
				if (dialog.getFile() != null) {
					String filename = dialog.getFile();
					File f = new File(dialog.getDirectory(), filename.endsWith(".txt") ? filename : filename + ".txt");
					Main.this.puzzle.saveHints(f);
				}
			}
		});
		puzzleMenu.add(saveHints);

		JMenuItem loadHints = new JMenuItem("Load hints");
		loadHints.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				FileDialog dialog = new FileDialog(window, "Select the hints to load.", FileDialog.LOAD);
				dialog.setFilenameFilter(new FilenameFilter() {
					@Override
					public boolean accept(File dir, String name) {
						return name.endsWith(".txt");
					}
				});
				dialog.setVisible(true);
				if (dialog.getFile() != null) {
					File f = new File(dialog.getDirectory(), dialog.getFile());
					Main.this.puzzle.loadHints(f);
				}
			}
		});
		puzzleMenu.add(loadHints);

		JMenuItem puzFillBlack = new JMenuItem("Fill black");
		puzFillBlack.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.this.puzzle.fillColor(MColor.Black);
			}
		});
		puzzleMenu.add(puzFillBlack);

		JMenuItem puzFillWhite = new JMenuItem("Fill white");
		puzFillWhite.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.this.puzzle.fillColor(MColor.White);
			}
		});
		puzzleMenu.add(puzFillWhite);

		JMenuItem puzFillGray = new JMenuItem("Fill gray");
		puzFillGray.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.this.puzzle.fillColor(MColor.Gray);
			}
		});
		puzzleMenu.add(puzFillGray);

		JMenuItem puzFillObvious = new JMenuItem("Fill 9 & 0");
		puzFillObvious.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.this.puzzle.fillObvious();
			}
		});
		puzzleMenu.add(puzFillObvious);

		// Generate menu

		JMenu generateMenu = new JMenu("Generate");
		menubar.add(generateMenu);

		JMenuItem genHints = new JMenuItem("Hints");
		genHints.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.this.puzzle.generateHints();
			}
		});
		generateMenu.add(genHints);

		JMenuItem genEmpty = new JMenuItem("Empty");
		genEmpty.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.this.ddialog.setVisible(true);
				if (Main.this.ddialog.isSuccess()) {
					int[] dim = Main.this.ddialog.getDimensions();
					Main.this.puzzle.newEmpty(dim[0], dim[1]);
				}
			}
		});
		generateMenu.add(genEmpty);

		JMenuItem genLizard = new JMenuItem("Lizard");
		genLizard.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.this.ddialog.setVisible(true);
				if (Main.this.ddialog.isSuccess()) {
					int[] dim = Main.this.ddialog.getDimensions();
					Main.this.puzzle.newFromGenerator(new LizardGenerator(dim[0], dim[1]), dim[0], dim[1]);
				}
			}
		});
		generateMenu.add(genLizard);

		JMenuItem genCloud = new JMenuItem("Cloud");
		genCloud.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.this.ddialog.setVisible(true);
				if (Main.this.ddialog.isSuccess()) {
					int[] dim = Main.this.ddialog.getDimensions();
					Main.this.puzzle.newFromGenerator(new CloudGenerator(), dim[0], dim[1]);
				}
			}
		});
		generateMenu.add(genCloud);

		JMenuItem genNoise = new JMenuItem("Random");
		genNoise.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.this.ddialog.setVisible(true);
				if (Main.this.ddialog.isSuccess()) {
					int[] dim = Main.this.ddialog.getDimensions();
					Main.this.puzzle.newFromGenerator(new TNoiseGenerator(), dim[0], dim[1]);
				}
			}
		});
		generateMenu.add(genNoise);

		JMenuItem genDFT = new JMenuItem("Random DFT");
		genDFT.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.this.ddialog.setVisible(true);
				if (Main.this.ddialog.isSuccess()) {
					int[] dim = Main.this.ddialog.getDimensions();
					Main.this.puzzle.newFromGenerator(new DFTGenerator(), dim[0], dim[1]);
				}
			}
		});
		generateMenu.add(genDFT);

		// color button

		this.colorButton = new JButton();
		this.colorButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				selectColor(nextColor(selectedColor));
			}
		});
		menubar.add(this.colorButton);

		// show error checkbox

		showMistakes = new JCheckBox("show mistakes");
		showMistakes.addItemListener(new ItemListener() {
			@Override
			public void itemStateChanged(ItemEvent e) {
				Main.this.puzzle.setShowMistakes(e.getStateChange() == 1);
			}
		});
		menubar.add(showMistakes);
		
		JButton undoButton = new JButton("undo");
		undoButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.this.puzzle.undo();
			}
		});
		menubar.add(undoButton);
		
		JButton redoButton = new JButton("redo");
		redoButton.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				Main.this.puzzle.redo();
			}
		});
		menubar.add(redoButton);
		

		// puzzle
		this.puzzle = new Puzzle();
		this.scroller = new JScrollPane(this.puzzle);
		this.scroller.setBorder(BorderFactory.createEmptyBorder(10, 10, 10, 10));

		this.add(this.scroller);

		this.puzzle.newEmpty(20, 20);
		this.setVisible(true);
	}

	private void registerShortcuts() {
		this.addWindowListener(this);
		InputMap im = this.getRootPane().getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);
		ActionMap am = this.getRootPane().getActionMap();

		// QUIT
		im.put(KeyStroke.getKeyStroke("ctrl Q"), "quit");
		im.put(KeyStroke.getKeyStroke("aft F4"), "quit");
		am.put("quit", new AbstractAction() {
			private static final long serialVersionUID = 1L;

			@Override
			public void actionPerformed(ActionEvent e) {
				Main.this.dispose();
			}
		});

		// new random puzzle
		im.put(KeyStroke.getKeyStroke("ctrl R"), "random");
		am.put("random", new AbstractAction() {
			private static final long serialVersionUID = 2L;

			@Override
			public void actionPerformed(ActionEvent e) {
				Main.this.ddialog.setVisible(true);
				if (Main.this.ddialog.isSuccess()) {
					int[] dim = Main.this.ddialog.getDimensions();
					Main.this.puzzle.newFromGenerator(new TNoiseGenerator(), dim[0], dim[1]);
				}
			}
		});

		// fill obvious
		im.put(KeyStroke.getKeyStroke("ctrl O"), "fillObvious");
		am.put("fillObvious", new AbstractAction() {
			private static final long serialVersionUID = 2L;

			@Override
			public void actionPerformed(ActionEvent e) {
				Main.this.puzzle.fillObvious();
			}
		});
	}
	
	public static void applySavedState(int new_width, int new_height, boolean show_mistakes) {
		window.ddialog.setDimensions(new_width, new_height);
		window.showMistakes.setSelected(show_mistakes);
	}

	/**
	 * This is the dialog to get the new width and height. Use
	 * {@code <dialog>.isSuccess()} to test whether it got cancelled or not.
	 */
	private class DimensionDialog extends JDialog {
		private static final long serialVersionUID = -7895915820382457183L;
		private JTextField width, height;
		private boolean success;

		public DimensionDialog(JFrame parent) {
			super(parent, "Enter the dimensions:", true);
			this.setLayout(new GridLayout(3, 2));
			this.setSize(new Dimension(200, 100));

			this.add(new JLabel("Width:"));
			this.width = new JTextField("20");
			this.add(this.width);
			this.add(new JLabel("Height:"));
			this.height = new JTextField("20");
			this.add(this.height);

			JButton enterButton = new JButton("Ok");
			enterButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					DimensionDialog.this.complete(true);
				}
			});
			this.add(enterButton);
			JButton cancelButton = new JButton("Cancel");
			cancelButton.addActionListener(new ActionListener() {
				@Override
				public void actionPerformed(ActionEvent e) {
					DimensionDialog.this.complete(false);
				}
			});
			this.add(cancelButton);

			this
					.getRootPane()
					.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
					.put(KeyStroke.getKeyStroke("pressed ENTER"), "enter");
			this.getRootPane().getActionMap().put("enter", new AbstractAction() {
				private static final long serialVersionUID = 8L;

				@Override
				public void actionPerformed(ActionEvent e) {
					DimensionDialog.this.complete(true);
				}
			});

			this
					.getRootPane()
					.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW)
					.put(KeyStroke.getKeyStroke("pressed ESCAPE"), "close");
			this.getRootPane().getActionMap().put("close", new AbstractAction() {
				private static final long serialVersionUID = 9L;

				@Override
				public void actionPerformed(ActionEvent e) {
					DimensionDialog.this.complete(false);
				}
			});
		}

		private void complete(boolean success) {
			this.success = success;
			this.setVisible(false);
		}

		public boolean isSuccess() {
			return this.success;
		}

		/**
		 * Gets the entered dimensions.
		 * 
		 * @return - The dimensions in the form {x, y}
		 */
		public int[] getDimensions() {
			return new int[] { Integer.parseInt(this.width.getText()), Integer.parseInt(this.height.getText()) };
		}
		
		public void setDimensions(int width, int height) {
			this.width.setText(Integer.toString(width));
			this.height.setText(Integer.toString(height));
		}
	}

	@Override
	public void windowClosed(WindowEvent e) {
		this.puzzle.save(StateIO.getDefaultFile(), this.ddialog.getDimensions());
		this.dispose();
		System.exit(0);
	}

	// I don't know why it doesn't exit properly else.
	@Override
	public void windowClosing(WindowEvent e) {
		this.dispose();
	}

	// We don't need all the following window events:
	@Override
	public void windowOpened(WindowEvent e) {
	}

	@Override
	public void windowIconified(WindowEvent e) {
	}

	@Override
	public void windowDeiconified(WindowEvent e) {
	}

	@Override
	public void windowActivated(WindowEvent e) {
	}

	@Override
	public void windowDeactivated(WindowEvent e) {
	}
}
