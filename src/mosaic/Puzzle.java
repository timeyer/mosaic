package mosaic;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseMotionListener;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;

import javax.swing.*;

import mosaic.area.Area;
import mosaic.area.AreaRect;
import mosaic.imagegenerators.ImageGenerator;
import mosaic.imagegenerators.LineGenerator;
import mosaic.imagegenerators.LineGenerator.Line;
import mosaic.util.HintGeneration;
import mosaic.util.HintIO;
import mosaic.util.StateIO;
import mosaic.util.Tuple;

/**
 * The GUI for the puzzle. See {@code Mosaic} for the main logic.
 * 
 * @author Timon
 * 
 * @see Mosaic
 *
 */
public class Puzzle extends JPanel implements MouseListener, MouseMotionListener {

	private static final long serialVersionUID = 4837438431412877474L;

	private int[][] hints;
	private Mosaic state;
	private int width, height;
	private int cellSize;
	private ImageGenerator generator;
	private ActionQueue actionQueue = new ActionQueue();

	private int[] startPos = { 0, 0 };
	private int[] lastClicked = { -1, -1 };
	private int[] llClicked = { -1, -1 };
	private int[] lastDragged = { -1, -1 };
	private MColor primary = MColor.Black, secondary = MColor.White;

	private boolean showMistakes = false;
	private ArrayList<int[]> mistakes;

	public Puzzle() {
		this.addMouseListener(this);
		this.addMouseMotionListener(this);
		this.mistakes = new ArrayList<>(0);
	}

	public void load(File file) {
		try {
			Tuple<Mosaic, int[][]> tuple = StateIO.LoadState(file);
			this.hints = tuple.b;
			this.state = tuple.a;
			this.width = this.state.getWidth();
			this.height = this.state.getHeight();
			this.actionQueue.clear();
			this.setPreferredSize(new Dimension(20 * width, 20 * height));
			this.verify();
			this.repaint();

		} catch (FileNotFoundException e) {
			System.out.println("Unable to load state:");
			e.printStackTrace();
		}
	}

	public void save(File file, int[] dimensions) {
		StateIO.SaveState(this.state, this.hints, dimensions, this.showMistakes, file);
	}

	public void generateHints() {
		try {
			this.hints = HintGeneration.generate(this.state);
			this.repaint();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void saveHints(File file) {
		HintIO.SaveHints(this.hints, file);
	}

	public void loadHints(File file) {
		try {
			this.hints = HintIO.LoadHints(file);
			this.height = this.hints.length;
			assert (this.height > 0);
			this.width = this.hints[0].length;
			this.state = new Mosaic(this.width, this.height);
			this.actionQueue.clear();
			this.verify();
			this.setPreferredSize(new Dimension(20 * width, 20 * height));
			this.repaint();
		} catch (FileNotFoundException e) {
			System.out.println("Unable to load hints from file " + file.getPath());
		}
	}

	/**
	 * Fills the entire field with the specified color.
	 * 
	 * @param color - The color to use
	 */
	public void fillColor(MColor color) {
		new AreaRect(0, 0, width - 1, height - 1).fill(this.state, color);
		this.verify();
		this.repaint();
	}

	/**
	 * Fills in the obvious fields around the hints 9 and 0
	 */
	public void fillObvious() {
		for (int j = 0; j < this.height; ++j) {
			for (int i = 0; i < this.width; ++i) {
				MColor c = MColor.Gray;
				if (hints[j][i] == 0)
					c = MColor.White;
				else if (hints[j][i] == 9)
					c = MColor.Black;
				else if (hints[j][i] == 6 && ((j == 0 || j == this.height - 1) ^ (i == 0 || i == this.width - 1)))
					c = MColor.Black;
				else if (hints[j][i] == 4 && (j == 0 || j == this.height - 1) && (i == 0 || i == this.width - 1))
					c = MColor.Black;

				if (c != MColor.Gray) {
					Area area = new AreaRect(
							Math.max(i - 1, 0),
							Math.max(j - 1, 0),
							Math.min(i + 1, this.width - 1),
							Math.min(j + 1, this.height + 1));
					area.fill(this.state, c);
				}
			}
		}
		this.verify();
		this.repaint();
	}

	/**
	 * Creates a new empty mosaic of a fixed size.
	 * 
	 * @param width - The width of the mosaic.
	 * @param height - The height of the mosaic.
	 */
	public void newEmpty(int width, int height) {
		this.width = width;
		this.height = height;
		this.hints = HintGeneration.generateEmpty(width, height);
		this.state = new Mosaic(width, height);
		this.mistakes.clear();
		this.actionQueue.clear();
		this.setPreferredSize(new Dimension(20 * width, 20 * height));
		this.revalidate();
		this.repaint();
	}

	public void newFromGenerator(ImageGenerator generator, int width, int height) {
		try {
			this.hints = HintGeneration
					.generate(Mosaic.FromBitset(generator.generateImage(width, height), width, height));
			this.state = new Mosaic(width, height);
			this.width = width;
			this.height = height;
			this.actionQueue.clear();
			this.setPreferredSize(new Dimension(20 * width, 20 * height));
			this.generator = generator;
			this.verify();
			this.revalidate();
			this.repaint();
		} catch (Exception e) {
			System.out.println("Hint generation failed for generated image. This may happen sometimes.");
			e.printStackTrace();
		}
	}

	public void setShowMistakes(boolean value) {
		this.showMistakes = value;
		this.repaint();
	}

	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		this.draw(g);
	}

	private void draw(Graphics g) {
		// compute the cell size (TODO: only do on resize)
		this.cellSize = Math
				.max(Math.min(this.getWidth() / this.state.getWidth(), this.getHeight() / this.state.getHeight()), 20);
		g.setFont(new Font("Consolas", Font.BOLD, (int) (cellSize * 0.8)));

		// draw the fields
		for (int y = 0; y < this.height; ++y) {
			for (int x = 0; x < this.width; ++x) {
				MColor color = this.state.colorAt(x, y);
				g
						.setColor(
								Main
										.colorForFieldColor(
												this.mistakes.size() == 0 && color == MColor.Gray
														? MColor.White
														: color));
				g.fillRect(x * this.cellSize, y * this.cellSize, this.cellSize, this.cellSize);

				// switch to white hint color if the field is black
				if (g.getColor() == Color.black)
					g.setColor(Color.white);
				else
					g.setColor(Color.black);

				if (hints[y][x] != -1) {
					g
							.drawString(
									Integer.toString(hints[y][x]),
									x * this.cellSize + (int) (this.cellSize * 0.3),
									(y + 1) * this.cellSize - (int) (this.cellSize * 0.2));
				}
			}
		}

		// draw grid over fields
		g.setColor(Color.gray);
		for (int i = 0; i <= this.state.getHeight(); ++i) {
			g.drawLine(0, i * this.cellSize, this.state.getWidth() * this.cellSize, i * this.cellSize);
		}
		for (int i = 0; i <= this.state.getWidth(); ++i) {
			g.drawLine(i * this.cellSize, 0, i * this.cellSize, this.state.getHeight() * this.cellSize);
		}

		// draw mistakes over old numbers for simplicity
		if (this.showMistakes) {
			g.setColor(Color.red);
			for (int[] pos : this.mistakes) {
				g
						.drawString(
								Integer.toString(hints[pos[1]][pos[0]]),
								pos[0] * this.cellSize + (int) (this.cellSize * 0.3),
								(pos[1] + 1) * this.cellSize - (int) (this.cellSize * 0.2));
			}
		}

		// line generator debugging
		if (this.generator != null && this.generator instanceof LineGenerator) {
			g.setColor(Color.red);
			for (Line l : ((LineGenerator) this.generator).getLines()) {
				l.drawToGraphics(g, this.cellSize);
			}
		}
	}

	/**
	 * Gets the grid position based on the mouse position.
	 * 
	 * @param e - The {@code MouseEvent}
	 * @return The coordinates in the grid or {-1, -1} if the mouse is outside
	 *         the grid.
	 */
	private int[] getCell(MouseEvent e) {
		int x = Math.min(e.getX() / this.cellSize, this.width);
		int y = Math.min(e.getY() / this.cellSize, this.height);
		if (x == this.width || y == this.height)
			return new int[] { -1, -1 };
		return new int[] { x, y };
	}

	/**
	 * Computes the mistakes and sets the mainframe color depending on the
	 * result.
	 */
	public void verify() {
		this.mistakes = this.state.verifyHints(this.hints);
		if (this.mistakes.size() == 0)
			Main.setFrameColor(Color.green);
		else
			Main.setFrameColor(Color.white);
	}

	private static boolean samePos(int[] a, int[] b) {
		return a[0] == b[0] && a[1] == b[1];
	}

	public void undo() {
		this.actionQueue.undo(this.state, this.hints);
		this.verify();
		this.repaint();
	}

	public void redo() {
		this.actionQueue.redo(this.state, this.hints);
		this.verify();
		this.repaint();
	}

	/////////////////////
	// EVENT LISTENERS //
	/////////////////////

	@Override
	public void mouseClicked(MouseEvent e) {
	}

	@Override
	public void mousePressed(MouseEvent e) {
		this.startPos = this.getCell(e);
	}

	@Override
	public void mouseReleased(MouseEvent e) {
		int[] endPos = this.getCell(e);

		if (samePos(this.startPos, endPos) && !samePos(this.lastDragged, endPos) && endPos[0] != -1) {
			// Color-loop: gray -> black, (black / white -> other), (white/black -> gray)
			if (this.state.colorAt(endPos[0], endPos[1]) == Main.selectedColor()) {
				if (samePos(llClicked, endPos)) {
					Main.selectColor(MColor.Gray);
				} else if (this.state.colorAt(endPos[0], endPos[1]) == this.primary) {
					Main.selectColor(this.secondary);
				} else {
					Main.selectColor(this.primary);
				}
			}
			
			this.actionQueue.drewAt(endPos[0], endPos[1], this.state.colorAt(endPos[0], endPos[1]), Main.selectedColor());
			this.state.setColorAt(endPos[0], endPos[1], Main.selectedColor());
			
			// begin color-loop anew if we ended on gray
			if (this.state.colorAt(endPos[0], endPos[1]) == MColor.Gray)
				this.lastClicked = this.llClicked = new int[] { -1, -1 };
			else {
				this.llClicked = this.lastClicked;
				this.lastClicked = endPos;
			}
			this.repaint();
			this.verify();
		}
		this.lastDragged = new int[] { -1, -1 };
	}

	@Override
	public void mouseDragged(MouseEvent e) {
		int[] pos = this.getCell(e);
		if (!samePos(this.lastDragged, pos) && !samePos(pos, this.startPos) && this.startPos[0] != -1 && pos[0] != -1) {
			this.lastDragged = pos;
			Main.selectColor(this.state.colorAt(this.startPos[0], this.startPos[1]));
			
			// prevent redrawn fields to not produce "invisible undo's"
			if (Main.selectedColor() != this.state.colorAt(pos[0], pos[1])) {
				if (Main.selectedColor() == MColor.Gray) {
					if (this.state.colorAt(pos[0], pos[1]) == MColor.Black) {
						this.primary = MColor.White; this.secondary = MColor.Black;
					} else {
						this.primary = MColor.Black; this.secondary = MColor.White;
					}
				}
				this.actionQueue.drewAt(pos[0], pos[1], this.state.colorAt(pos[0], pos[1]), Main.selectedColor());
				this.state.setColorAt(pos[0], pos[1], Main.selectedColor());

				this.verify();		
			}
		}
	}

	// We don't need the following events:
	@Override
	public void mouseEntered(MouseEvent e) {
	}

	@Override
	public void mouseExited(MouseEvent e) {
	}

	@Override
	public void mouseMoved(MouseEvent e) {
	}
}
