package mosaic;

public class Field {
	public MColor color;
	public int number;

	public Field() {
		this.color = MColor.Gray;
		this.number = 0;
	}

	public Field(MColor color) {
		this.color = color;
		this.number = 0;
	}
}
