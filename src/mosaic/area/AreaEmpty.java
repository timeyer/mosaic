package mosaic.area;

import mosaic.MColor;
import mosaic.Mosaic;
import mosaic.util.Bitset2D;

public class AreaEmpty extends Area {

	@Override
	public AreaStatistics statistics(Mosaic input, Bitset2D forced) {
		return new AreaStatistics(0, 0, 0, 0);
	}

	@Override
	public void force(Bitset2D forced) {
	}

	@Override
	public void fill(Mosaic mosaic, MColor color) {
	}

}
