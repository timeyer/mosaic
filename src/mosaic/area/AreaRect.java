package mosaic.area;

import mosaic.MColor;
import mosaic.Mosaic;
import mosaic.util.Bitset2D;

/**
 * A simple rectangular area.
 * 
 * @author Timon
 *
 */
public class AreaRect extends Area {
	private final int lx, ly, ux, uy;

	public AreaRect(int lx, int ly, int ux, int uy) {
		this.lx = lx;
		this.ly = ly;
		this.ux = ux;
		this.uy = uy;
	}

	@Override
	public AreaStatistics statistics(Mosaic input, Bitset2D forced) {
		int sx = Math.max(0, lx);
		int sy = Math.max(0, ly);
		int ex = Math.min(input.getWidth(), ux + 1);
		int ey = Math.min(input.getHeight(), uy + 1);
		int fw = 0, fb = 0;
		int affected = (ex - sx) * (ey - sy);
		// count forced fields around (fw = forced white, fb = forced black)
		for (int i = sy; i < ey; ++i) {
			for (int j = sx; j < ex; ++j) {
				if (forced.get(j, i)) {
					if (input.colorAt(j, i) == MColor.Black)
						fb += 1;
					else // if (input.colorAt(j, i) == MColor.White)
						fw += 1;
				}
			}
		}

		// how many are not forced yet
		int nf = affected - fw - fb;
		return new AreaStatistics(affected, nf, fb, fw);
	}

	@Override
	public void force(Bitset2D forced) {
		for (int y = Math.max(0, ly); y < Math.min(forced.height, uy + 1); ++y) {
			for (int x = Math.max(0, lx); x < Math.min(forced.width, ux + 1); ++x) {
				forced.set(x, y);
			}
		}
	}

	@Override
	public void fill(Mosaic mosaic, MColor color) {
		int width = mosaic.getWidth();
		int height = mosaic.getHeight();
		for (int y = Math.max(0, ly); y < Math.min(height, uy + 1); ++y) {
			for (int x = Math.max(0, lx); x < Math.min(width, ux + 1); ++x) {
				mosaic.setColorAt(x, y, color);
			}
		}
	}
}
