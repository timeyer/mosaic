package mosaic.area;

import mosaic.MColor;
import mosaic.Mosaic;
import mosaic.util.Bitset2D;

/**
 * AreaCombination combines multiple **non-overlapping** areas.
 * 
 * @author Timon
 *
 */
public class AreaUnion extends Area {
	private Area[] subregions;

	/**
	 * Creates a new combined area from subregions.
	 * 
	 * @param subregions - The **non-overlapping** subregions.
	 */
	public AreaUnion(Area[] subregions) {
		this.subregions = subregions;
	}

	@Override
	public AreaStatistics statistics(Mosaic input, Bitset2D forced) {
		AreaStatistics s = new AreaStatistics(0, 0, 0, 0);
		for (Area a : subregions) {
			AreaStatistics sn = a.statistics(input, forced);
			s = new AreaStatistics(
					s.size + sn.size,
					s.free + sn.free,
					s.forcedBlack + sn.forcedBlack,
					s.forcedWhite + sn.forcedWhite);
		}
		return s;
	}

	@Override
	public void force(Bitset2D forced) {
		for (Area a : subregions) {
			a.force(forced);
		}
	}

	@Override
	public void fill(Mosaic mosaic, MColor color) {
		for (Area a : subregions) {
			a.fill(mosaic, color);
		}
	}

}
