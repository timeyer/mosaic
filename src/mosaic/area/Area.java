package mosaic.area;

import mosaic.MColor;
import mosaic.Mosaic;
import mosaic.util.Bitset2D;

/**
 * Defines an area over a mosaic to get statistics about the markings and to run
 * operations over the whole area.
 * 
 * @author Timon
 */
public abstract class Area {
	public abstract AreaStatistics statistics(Mosaic input, Bitset2D forced);

	public abstract void force(Bitset2D forced);

	public abstract void fill(Mosaic mosaic, MColor color);
}
