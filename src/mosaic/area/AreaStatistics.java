package mosaic.area;

/**
 * Statistics over a specific area of some mosaic.
 * 
 * @author Timon
 *
 */
public class AreaStatistics {
	public final int size, free, forcedBlack, forcedWhite;

	public AreaStatistics(int s, int nf, int fb, int fw) {
		this.size = s;
		this.free = nf;
		this.forcedBlack = fb;
		this.forcedWhite = fw;
	}
}
