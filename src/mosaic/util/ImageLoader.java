package mosaic.util;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import mosaic.MColor;
import mosaic.Mosaic;

/**
 * ImageLoader contains functions for loading and saving mosaics to disk.
 * Loading a partial state is currently not implemented.
 * 
 * @author Timon
 *
 */

public class ImageLoader {
	/**
	 * Loads a new mosaic from a file.
	 * 
	 * @param path - The file path
	 * @return The mosaic from the image
	 * @throws IOException
	 */
	public static Mosaic loadNew(String path) throws IOException {
		BufferedImage image = ImageIO.read(new File(path));
		Mosaic solution = new Mosaic(image.getWidth(), image.getHeight());
		int sx = image.getMinX();
		int sy = image.getMinY();
		int ex = sx + image.getWidth();
		int ey = sy + image.getHeight();

		for (int x = sx; x < ex; ++x) {
			for (int y = sy; y < ey; ++y) {
				solution.setColorAt(x, y, readPixel(x, y, image));
			}
		}

		return solution;
	}

	/**
	 * Reads a pixel from the image and thresholds it. (Yes, we convert a Gray
	 * value to RGB for that if necessary)
	 * 
	 * @param x - The x position in the image
	 * @param y - The y position in the image
	 * @param image - The image holding the colors
	 * @return The thresholded color
	 */
	private static MColor readPixel(int x, int y, BufferedImage image) {
		int value = image.getRGB(x, y);
		return (value & 0xff + value & 0xff00 + value & 0xff0000) >= 384 ? MColor.White : MColor.Black;
	}
}
