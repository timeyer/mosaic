package mosaic.util;

/**
 * A 2D bitset built of a table of longs.
 * 
 * @author Timon
 * 
 *         each row contains as many longs as necessary, filled from left to
 *         right.
 * 
 *         Example of a fully set (all values are true) 2x2 bitset: | x\y |
 *         0..63 | | 0 | 0b110000... | | 1 | 0b110000... |
 *
 */
public class Bitset2D {
	private long[][] arr;
	public final int width;
	private int arr_width;
	public final int height;

	public Bitset2D(int w, int h) {
		this.arr_width = Math.max(0, w - 1) / 64 + 1;
		this.arr = new long[h][this.arr_width];
		for (int y = 0; y < h; ++y) {
			this.arr[y] = new long[this.arr_width];
		}
		this.width = w;
		this.height = h;
	}

	public boolean get(int x, int y) {
		return (this.arr[y][x / 64] & (1L << (63 - x % 64))) != 0;
	}

	public void set(int x, int y, boolean value) {
		if (value)
			this.set(x, y);
		else
			this.unset(x, y);
	}

	public void set(int x, int y) {
		this.arr[y][x / 64] |= 1L << (63 - x % 64);
	}

	public void unset(int x, int y) {
		this.arr[y][x / 64] ^= this.arr[y][x / 64] & (1L << (63 - x % 64));
	}

	public void toggle(int x, int y) {
		this.arr[y][x / 64] ^= 1L << (63 - x % 64);
	}

	public boolean allSet() {
		for (int y = 0; y < this.height; ++y) {
			for (int x = 0; x < this.arr_width; ++x) {
				// All fully used longs of a row need to be 0b111... = ~0
				if ((x < this.arr_width - 1) && this.arr[y][x] != ~0)
					return false;

				// the last long of a row is a bit more complicated:
				// We shift 0b10000... arithmetically (signed long) to the right
				// to compare.
				// Therefore we shift by one less than width % 64, unless we use
				// the full
				// byte for storage. There we'd get 0 but have to shift 63 bits.
				else if ((x == this.arr_width - 1)
						&& (this.arr[y][x] != Long.MIN_VALUE >> (this.width % 64 == 0 ? 63 : this.width % 64 - 1)))
					return false;
			}
		}
		return true;
	}

	public void setAll() {
		for (int y = 0; y < this.height; ++y) {
			for (int x = 0; x < this.arr_width; ++x) {
				// All fully used longs of a row need to be 0b111... = ~0
				if (x < this.arr_width - 1)
					this.arr[y][x] = ~0;

				// for the last long in the row we do the same as in {@code
				// allSet}.
				else
					this.arr[y][x] = Long.MIN_VALUE >> (this.width % 64 == 0 ? 63 : this.width % 64 - 1);
			}
		}
	}
}
