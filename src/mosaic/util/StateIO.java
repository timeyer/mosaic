package mosaic.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

import mosaic.MColor;
import mosaic.Main;
import mosaic.Mosaic;

/**
 * Handles reading and writing mosaic state from/to files.
 * 
 * The format is as follows: First line are two integers separated by a space
 * denoting the width and height of the mosaic.
 * 
 * The following height lines contain the rows of the matrix, a space denoting that
 * there is no hint for this position and a digit if the hint is given.
 * 
 * Then the last height lines indicate the color of the fields, 0 for black, 1 for gray and 2 for white
 * 
 * @author Timon
 *
 */
public class StateIO {

	public static File getDefaultFile() {
		String OS = (System.getProperty("os.name")).toUpperCase();
		String path;
		if (OS.contains("WIN")) {
			path = System.getenv("AppData") + '\\' + "mosaic.txt";
		} else if (OS.contains("LINUX")) {
			path = System.getProperty("user.home") + "/.local/share/mosaic.txt";
		} else {
			path = System.getProperty("user.home") + "/Library/Application Support/mosaic.txt";
		}
		return new File(path);
	}
	
	/**
	 * Writes the state to the file specified by path.
	 * 
	 * @param mosaic - The mosaic to write to the file
	 * @param hints - The hints to write to the file
	 * @param path - The path to the file
	 */
	public static void SaveState(Mosaic mosaic, int[][] hints, int[] new_dimensions, boolean showMistakes, File file) {
		assert (hints.length != 0);
		try (FileWriter out = new FileWriter(file)) {
			int width = mosaic.getWidth();
			int height = mosaic.getHeight();
			out.write(width + " " + height + " " + new_dimensions[0] + " " + new_dimensions[1] + " " + (showMistakes ? "Show" : "Hide") + "\n");
			for (int y = 0; y < height; ++y) {
				for (int x = 0; x < width; ++x) {
					if (hints[y][x] == -1)
						out.write(' ');
					else
						out.write(Integer.toString(hints[y][x]));
				}
				out.write('\n');
			}
			for (int y = 0; y < height; ++y) {
				for (int x = 0; x < width; ++x) {
					if (mosaic.colorAt(x, y) == MColor.Black) {
						out.write('0');
					} else if (mosaic.colorAt(x, y) == MColor.Gray) {
						out.write('1');
					} else {
						out.write('2');
					}
				}
				out.write('\n');
			}
			out.flush();
			out.close();

		} catch (IOException e) {
			System.out.println("Unable to write hint file:");
			e.printStackTrace();
		}
	}

	/**
	 * Reads the hints from the file specified by path.
	 * 
	 * @param path - The path to the file
	 * @return The mosaic and hint matrix
	 * @throws FileNotFoundException
	 */
	public static Tuple<Mosaic, int[][]> LoadState(File file) throws FileNotFoundException {
		try (Scanner s = new Scanner(file)) {
			String[] l1 = s.nextLine().stripTrailing().split(" ");
			int w = Integer.parseInt(l1[0]);
			int h = Integer.parseInt(l1[1]);
			// later addition, may be missing:
			try {
				int new_w = Integer.parseInt(l1[2]);
				int new_h = Integer.parseInt(l1[3]);
				boolean showMistakes = l1[4].equals("Show");
				Main.applySavedState(new_w, new_h, showMistakes);
			} catch (Exception e) {}
			
			int[][] hints = new int[h][w];

			for (int y = 0; y < h; ++y) {
				String line = s.nextLine();
				for (int x = 0; x < w; ++x) {
					if (line.charAt(x) == ' ')
						hints[y][x] = -1;
					else
						// all encodings have the digits following each other
						hints[y][x] = line.charAt(x) - '0';
				}
			}
			Mosaic mosaic = new Mosaic(w, h);
			for (int y = 0; y < h; ++y) {
				String line = s.nextLine();
				for (int x = 0; x < w; ++x) {
					if (line.charAt(x) == '0')
						mosaic.setColorAt(x, y, MColor.Black);
					else if (line.charAt(x) == '2')
						mosaic.setColorAt(x, y, MColor.White);
				}
			}
			return new Tuple<>(mosaic, hints);
		} catch (FileNotFoundException e) {
			throw e;
		}
	}
}
