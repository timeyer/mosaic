package mosaic.util;

import mosaic.Mosaic;

/**
 * This class holds the hint generation algorithm(s) for a {@code Mosaic}.
 * 
 * @author Timon
 * 
 * @see Mosaic
 *
 */
public class HintGeneration {
	public static int[][] generateEmpty(int width, int height) {
		int[][] res = new int[height][width];
		for (int y = 0; y < height; ++y) {
			for (int x = 0; x < width; ++x) {
				res[y][x] = -1;
			}
		}
		return res;
	}

	public static int[][] generate(Mosaic input) throws Exception {
		int[][] result = new int[input.getHeight()][input.getWidth()];
		if (!generate(input, result))
			throw new Exception("Unable to generate hints for mosaic: not well-formed");

		return result;
	}

	public static boolean generate(Mosaic input, int[][] result) {
		int h = input.getHeight();
		if (h == 0)
			return true;

		int w = input.getWidth();
		Bitset2D forced = new Bitset2D(w, h);

		// we clear the hints here to have a blank slate.
		for (int j = 0; j < h; ++j) {
			for (int i = 0; i < w; ++i) {
				result[j][i] = -1;
			}
		}

		boolean iter_found = true;
		Hint bestHint;
		// int best_x = 0;
		// int best_y = 0;
		// int best_fc;
		// int sx, sy, ex, ey, fw, fb, affected, nf;

		// We find the most forcing square each iteration.
		// If we don't find any, we are finished.
		// (either the mosaic is not well-formed, or all fields are forced)
		while (iter_found) {
			iter_found = false;
			
			for (int y = 0; y < h; ++y) {
				for (int x = 0; x < w; ++x) {
					Hint sh = SingleHint.find(x, y, input, forced);
					if (result[y][x] != -1 && sh != null)
						sh.apply(result, forced);
				}
			}
			
			bestHint = new NonHint();
			for (int y = 0; y < h; ++y) {
				for (int x = 0; x < w; ++x) {
					bestHint = bestHint
							.choose(SingleHint.find(x, y, input, forced))
							.choose(DoubleHint.findAdjT(x, y, input, forced))
							.choose(DoubleHint.findAdjR(x, y, input, forced))
							.choose(DoubleHint.findDistT(x, y, input, forced))
							.choose(DoubleHint.findDistR(x, y, input, forced))
							.choose(DoubleHint.findDiagTL(x, y, input, forced))
							.choose(DoubleHint.findDiagTR(x, y, input, forced))
							.choose(DoubleHint.findDistDiagTL(x, y, input, forced))
							.choose(DoubleHint.findDistDiagTR(x, y, input, forced))
							.choose(DoubleHint.findDistDiagRT(x, y, input, forced))
							.choose(DoubleHint.findDistDiagRB(x, y, input, forced))
							;
				}
			}
			if (bestHint.forcedCount > 0) {
				iter_found = true;
				bestHint.apply(result, forced);
				// result[best_y][best_x] = input.numberAt(best_x, best_y);
				// for (int y = Math.max(0, best_y - 1); y < Math.min(h, best_y
				// + 2); ++y) {
				// for (int x = Math.max(0, best_x - 1); x < Math.min(w, best_x
				// + 2); ++x) {
				// forced.set(x, y);
				// }
				// }
			}
		}

		if (forced.allSet())
			return true;
		else
			return false;
	}
}
