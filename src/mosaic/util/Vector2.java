package mosaic.util;

public class Vector2 {
	private double x, y;

	public Vector2(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public static Vector2 FromPolar(double r, double theta) {
		return new Vector2(r * Math.cos(theta), r * Math.sin(theta));
	}

	public double getX() {
		return this.x;
	}

	public double getY() {
		return this.y;
	}

	public Vector2 add(Vector2 other) {
		return new Vector2(this.x + other.x, this.y + other.y);
	}

	public Vector2 scale(double f) {
		return new Vector2(this.x * f, this.y * f);
	}

	public double len() {
		return Math.abs(this.x * this.x + this.y * this.y);
	}

}
