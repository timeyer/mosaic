package mosaic.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

/**
 * Handles reading and writing hints from/to files.
 * 
 * The format is as follows: First line are two integers separated by a space
 * denoting the width and height of the hint matrix.
 * 
 * The following lines contain the rows of the matrix, a space denoting that
 * there is no hint for this position and a digit if the hint is given.
 * 
 * @author Timon
 *
 */
public class HintIO {

	/**
	 * Writes the hints to the file specified by path.
	 * 
	 * @param hints - The hints to write to the file
	 * @param path - The path to the file
	 */
	public static void SaveHints(int[][] hints, File file) {
		assert (hints.length != 0);
		try (FileWriter out = new FileWriter(file)) {
			out.write(hints[0].length + " " + hints.length + "\n");
			for (int y = 0; y < hints.length; ++y) {
				for (int x = 0; x < hints[0].length; ++x) {
					if (hints[y][x] == -1)
						out.write(' ');
					else
						out.write(Integer.toString(hints[y][x]));
				}
				out.write('\n');
			}
			out.flush();
			out.close();

		} catch (IOException e) {
			System.out.println("Unable to write hint file:");
			e.printStackTrace();
		}
	}

	/**
	 * Reads the hints from the file specified by path.
	 * 
	 * @param path - The path to the file
	 * @return The hint matrix
	 * @throws FileNotFoundException
	 */
	public static int[][] LoadHints(File file) throws FileNotFoundException {
		try (Scanner s = new Scanner(file)) {
			String[] l1 = s.nextLine().split(" ");
			int w = Integer.parseInt(l1[0]);
			int h = Integer.parseInt(l1[1]);
			int[][] hints = new int[h][w];

			for (int y = 0; y < h; ++y) {
				String line = s.nextLine();
				for (int x = 0; x < w; ++x) {
					if (line.charAt(x) == ' ')
						hints[y][x] = -1;
					else
						// all encodings have the digits following each other
						hints[y][x] = line.charAt(x) - '0';
				}
			}
			return hints;
		} catch (FileNotFoundException e) {
			throw e;
		}
	}

}
