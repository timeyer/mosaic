package mosaic.util;

public class Complex {
	public double real;
	public double imag;

	public Complex(double r, double i) {
		real = r;
		imag = i;
	}

	public Complex copy() {
		return new Complex(this.real, this.imag);
	}

	public Complex mul(Complex o) {
		return new Complex(this.real * o.real - this.imag * o.imag, this.real * o.imag + this.imag * o.real);
	}

	public Complex imul(Complex o) {
		double tro = this.real;
		double tio = this.imag;
		this.real = tro * o.real - tio * o.imag;
		this.imag = tro * o.imag + tio * o.real;
		return this;
	}

	public Complex mul(double f) {
		this.real *= f;
		this.imag *= f;
		return this;
	}

	public Complex iadd(Complex o) {
		this.real += o.real;
		this.imag += o.imag;
		return this;
	}

	public Complex add(Complex o) {
		return new Complex(this.real + o.real, this.imag + o.imag);
	}

	public Complex exp() {
		return new Complex(Math.exp(this.real) * Math.cos(this.imag), Math.exp(this.real) * Math.sin(this.imag));
	}
}
