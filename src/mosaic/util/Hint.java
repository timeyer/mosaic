package mosaic.util;

import mosaic.Mosaic;
import mosaic.area.Area;
import mosaic.area.AreaUnion;
import mosaic.area.AreaEmpty;
import mosaic.area.AreaRect;
import mosaic.area.AreaStatistics;

abstract class Hint {
	public final int forcedCount;

	Hint(int c) {
		forcedCount = c;
	}

	Hint choose(Hint other) {
		if (other == null || this.forcedCount >= other.forcedCount)
			return this;
		return other;
	}

	abstract void apply(int[][] hintmap, Bitset2D fm);
}

class NonHint extends Hint {
	NonHint() {
		super(0);
	}

	@Override
	void apply(int[][] field, Bitset2D fm) {
	}
}

class SingleHint extends Hint {
	private int x;
	private int y;
	private int n;

	SingleHint(int x, int y, int n, int f) {
		super(f);
		this.x = x;
		this.y = y;
		this.n = n;
	}

	static SingleHint find(int x, int y, Mosaic input, Bitset2D fm) {
		AreaStatistics stats = (new AreaRect(x - 1, y - 1, x + 1, y + 1)).statistics(input, fm);

		if (stats.forcedBlack + stats.free == input.numberAt(x, y)
				|| stats.forcedWhite + stats.free == stats.size - input.numberAt(x, y)) {
			return new SingleHint(x, y, input.numberAt(x, y), stats.free);
		} else {
			return null;
		}
	}

	@Override
	void apply(int[][] hintmap, Bitset2D fm) {
		hintmap[y][x] = n;
		(new AreaRect(x - 1, y - 1, x + 1, y + 1)).force(fm);
	}
}

class DoubleHint extends Hint {
	private int x1, y1, n1;
	private Area a1;
	private int x2, y2, n2;
	private Area a2;

	DoubleHint(int x1, int y1, int n1, Area a1, int x2, int y2, int n2, Area a2, int f) {
		super(f);
		this.x1 = x1;
		this.y1 = y1;
		this.n1 = n1;
		this.a1 = a1;
		this.x2 = x2;
		this.y2 = y2;
		this.n2 = n2;
		this.a2 = a2;
	}

	@Override
	void apply(int[][] hintmap, Bitset2D fm) {
		hintmap[y1][x1] = n1;
		a1.force(fm);
		hintmap[y2][x2] = n2;
		a2.force(fm);
	}

	static DoubleHint build(int x1, int y1, Area a1, int x2, int y2, Area a2, Area between, Mosaic input, Bitset2D fm) {
		int n1 = input.numberAt(x1, y1);
		int n2 = input.numberAt(x2, y2);
		AreaStatistics s1 = a1.statistics(input, fm);
		AreaStatistics s2 = a2.statistics(input, fm);
		AreaStatistics sb = between.statistics(input, fm);

		int ft = (n1 == Math.max(n2 - s2.forcedBlack, sb.free) + s1.free + s1.forcedBlack && s1.free > 0) ? s1.free : 0;
		int fb = (n2 == Math.max(n1 - s1.forcedBlack, sb.free) + s2.free + s2.forcedBlack && s2.free > 0) ? s2.free : 0;
		// one side was forced, the other one may be forced white now
		if (ft == 0 && fb > 0) {
			ft = (n1 == (n2 - s2.forcedBlack - fb) + s1.forcedBlack && s1.free > 0) ? s1.free : 0;
		} else if (fb == 0 && ft > 0) {
			fb = (n2 == (n1 - s1.forcedBlack - ft) + s2.forcedBlack && s2.free > 0) ? s2.free : 0;
		}
		if (ft > 0 || fb > 0) {
			Area a1f = ft > 0 ? a1 : new AreaEmpty();
			Area a2f = fb > 0 ? a2 : new AreaEmpty();
			return new DoubleHint(x1, y1, n1, a1f, x2, y2, n2, a2f, ft + fb);
		}
		return null;
	}

	static DoubleHint findAdjT(int x, int y, Mosaic input, Bitset2D fm) {
		if (y + 1 >= fm.height)
			return null;

		Area a1 = new AreaRect(x - 1, y - 1, x + 1, y - 1);
		Area a2 = new AreaRect(x - 1, y + 2, x + 1, y + 2);
		Area between = new AreaRect(x - 1, y, x + 1, y + 1);
		return build(x, y, a1, x, y + 1, a2, between, input, fm);
	}

	static DoubleHint findAdjR(int x, int y, Mosaic input, Bitset2D fm) {
		if (x + 1 >= fm.width)
			return null;

		Area a1 = new AreaRect(x - 1, y - 1, x - 1, y + 1);
		Area a2 = new AreaRect(x + 2, y - 1, x + 2, y + 1);
		Area between = new AreaRect(x, y - 1, x + 1, y + 1);
		return build(x, y, a1, x + 1, y, a2, between, input, fm);
	}

	static DoubleHint findDistT(int x, int y, Mosaic input, Bitset2D fm) {
		if (y + 2 >= fm.height)
			return null;

		Area a1 = new AreaRect(x - 1, y - 1, x + 1, y);
		Area a2 = new AreaRect(x - 1, y + 2, x + 1, y + 3);
		Area between = new AreaRect(x - 1, y + 1, x + 1, y + 1);
		return build(x, y, a1, x, y + 2, a2, between, input, fm);
	}

	static DoubleHint findDistR(int x, int y, Mosaic input, Bitset2D fm) {
		if (x + 2 >= fm.width)
			return null;

		Area a1 = new AreaRect(x - 1, y - 1, x, y + 1);
		Area a2 = new AreaRect(x + 2, y - 1, x + 3, y + 1);
		Area between = new AreaRect(x + 1, y - 1, x + 1, y + 1);
		return build(x, y, a1, x + 2, y, a2, between, input, fm);
	}

	static DoubleHint findDiagTR(int x, int y, Mosaic input, Bitset2D fm) {
		if (x + 1 >= fm.width || y + 1 >= fm.height)
			return null;

		Area a1 = new AreaUnion(
				new Area[] { new AreaRect(x - 1, y - 1, x + 1, y - 1), new AreaRect(x - 1, y, x - 1, y + 1) });
		Area a2 = new AreaUnion(
				new Area[] { new AreaRect(x, y + 2, x + 2, y + 2), new AreaRect(x + 2, y, x + 2, y + 1) });
		Area between = new AreaRect(x, y, x + 1, y + 1);
		return build(x, y, a1, x + 1, y + 1, a2, between, input, fm);
	}

	static DoubleHint findDiagTL(int x, int y, Mosaic input, Bitset2D fm) {
		if (x - 1 < 0 || y + 1 >= fm.height)
			return null;

		Area a1 = new AreaUnion(
				new Area[] { new AreaRect(x - 1, y - 1, x + 1, y - 1), new AreaRect(x + 1, y, x + 1, y + 1) });
		Area a2 = new AreaUnion(
				new Area[] { new AreaRect(x - 2, y + 2, x, y + 2), new AreaRect(x - 2, y, x - 2, y + 1) });
		Area between = new AreaRect(x - 1, y, x, y + 1);
		return build(x, y, a1, x - 1, y + 1, a2, between, input, fm);
	}

	static DoubleHint findDistDiagTL(int x, int y, Mosaic input, Bitset2D fm) {
		if (x - 1 < 0 || y + 2 >= fm.height) {
			return null;
		}

		Area a1 = new AreaUnion(
				new Area[] { new AreaRect(x - 1, y - 1, x + 1, y), new AreaRect(x + 1, y + 1, x + 1, y + 1) });
		Area a2 = new AreaUnion(
				new Area[] { new AreaRect(x - 2, y + 2, x, y + 3), new AreaRect(x - 2, y + 1, x - 2, y + 1) });
		Area between = new AreaRect(x - 1, y + 1, x, y + 1);
		return build(x, y, a1, x - 1, y + 2, a2, between, input, fm);
	}

	static DoubleHint findDistDiagTR(int x, int y, Mosaic input, Bitset2D fm) {
		if (x + 1 >= fm.width || y + 2 >= fm.height) {
			return null;
		}

		Area a1 = new AreaUnion(
				new Area[] { new AreaRect(x - 1, y - 1, x + 1, y), new AreaRect(x - 1, y + 1, x - 1, y + 1) });
		Area a2 = new AreaUnion(
				new Area[] { new AreaRect(x, y + 2, x + 2, y + 3), new AreaRect(x + 2, y + 1, x + 2, y + 1) });
		Area between = new AreaRect(x, y + 1, x + 1, y + 1);
		return build(x, y, a1, x + 1, y + 2, a2, between, input, fm);
	}

	static DoubleHint findDistDiagRT(int x, int y, Mosaic input, Bitset2D fm) {
		if (x + 2 >= fm.width || y + 1 >= fm.height) {
			return null;
		}

		Area a1 = new AreaUnion(
				new Area[] { new AreaRect(x - 1, y - 1, x, y + 1), new AreaRect(x + 1, y - 1, x + 1, y - 1) });
		Area a2 = new AreaUnion(
				new Area[] { new AreaRect(x + 2, y, x + 3, y + 2), new AreaRect(x + 1, y + 2, x + 1, y + 2) });
		Area between = new AreaRect(x + 1, y, x + 1, y + 1);
		return build(x, y, a1, x + 2, y + 1, a2, between, input, fm);
	}

	static DoubleHint findDistDiagRB(int x, int y, Mosaic input, Bitset2D fm) {
		if (x + 2 >= fm.width || y - 1 < 0) {
			return null;
		}

		Area a1 = new AreaUnion(
				new Area[] { new AreaRect(x - 1, y - 1, x, y + 1), new AreaRect(x + 1, y + 1, x + 1, y + 1) });
		Area a2 = new AreaUnion(
				new Area[] { new AreaRect(x + 2, y - 2, x + 3, y), new AreaRect(x + 1, y - 2, x + 1, y - 2) });
		Area between = new AreaRect(x + 1, y - 1, x + 1, y);
		return build(x, y, a1, x + 2, y - 1, a2, between, input, fm);
	}
}
